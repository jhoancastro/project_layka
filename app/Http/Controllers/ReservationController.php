<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation as Reservation;

class ReservationController extends Controller
{
    public function index()
	{
		$reservations = Reservation::all();
		return \View::make('reservation/list', compact('reservations'));

	}
	public function store(Request $request)
	{
		$reservation = new Reservation;
	    $reservation->datere = $request->datere;
	    $reservation->restaurant = $request->restaurant;
	    $reservation->save();
	    return redirect('reservation');
	}

	public function create()
	{
		return \View::make('reservation/new');
	}

	public function edit($id)
	{
		$reservation = Reservation::find($id);
		return \View::make('reservation/update', compact('reservation'));
	}

	public function update($id, Request $request)
	{
		$reservation = Reservation::find($id);
		$reservation->datere = $request->datere;
		$reservation->restaurant = $request->restaurant;
		$reservation->save();
		return redirect('reservation');
	}

	public function show(Request $request)
	{
		$reservations = Reservation::where('restaurant','like','%'.$request->restaurant.'%')->get();
		return \View::make('reservation/list' ,compact('reservations'));
	}

	public function destroy($id)
	{
		$reservation = Reservation::find($id);
		$reservation->delete();
		return redirect()->back();
	}
}

