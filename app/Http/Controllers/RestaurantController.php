<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant as Restaurant;

class RestaurantController extends Controller
{
    public function index()
	{
		$restaurants = Restaurant::all();
		return \View::make('restaurants/list', compact('restaurants'));
	}
	public function store(Request $request)
	{
		$file = $request->photo;
        $path = public_path().'/images/';
        $fileName = $file->getClientOriginalName();

      	$file->move($path, $fileName);

		$restaurant = new Restaurant;
	    $restaurant->name = $request->name;
	    $restaurant->description = $request->description;
	    $restaurant->adress = $request->adress;
	    $restaurant->city = $request->city;
	    $restaurant->photo = $fileName;
	    $restaurant->save();
	    return redirect('restaurant');
	}

	public function create()
	{
		return \View::make('restaurants/new');
	}

	public function edit($id)
	{
		$restaurant = Restaurant::find($id);
		return \View::make('restaurants/update', compact('restaurant'));
	}

	public function update($id, Request $request)
	{
		$restaurant = Restaurant::find($id);
		$restaurant->name = $request->name;
		$restaurant->description = $request->description;
		$restaurant->adress = $request->adress;
	    $restaurant->city = $request->city;
	   // $restaurant->photo = $request->photo;
		$restaurant->save();
		return redirect('restaurant');
	}

	public function show(Request $request)
	{
		if ($request->city != null) {
			$restaurants = Restaurant::where('city','=',$request->city)->get();
		}else{
			$restaurants = Restaurant::where('name','like','%'.$request->name.'%')->get();
		}
		
		return \View::make('restaurants/list' ,compact('restaurants'));
	}

	public function destroy($id)
	{
		$restaurant = Restaurant::find($id);
		$restaurant->delete();
		return redirect()->back();
	}
}
