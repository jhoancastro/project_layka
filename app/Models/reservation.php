<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class reservation extends Model
{
    Protected $table = 'reservations';
    protected $fillable = ['datere', 'restaurant'];
    protected $guarded = ['id'];
}
