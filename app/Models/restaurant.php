<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class restaurant extends Model
{
    Protected $table = 'restaurants';
    protected $fillable = ['name', 'description', 'adress', 'city', 'photo'];
    protected $guarded = ['id'];
}
