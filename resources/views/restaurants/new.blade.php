@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'restaurant.store', 'method' => 'post','files' => true, 'novalidate' ]) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Descripcion</label>
						<input type="text" name="description" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Direccion</label>
						<input type="text" name="adress" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Ciudad</label>
						<input type="text" name="city" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Foto</label>
						<input type="file" name="photo" class="form-control" required>
					</div>
					<div class="form-group">
						<button type="subit" class="btn btn-success">Registar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection