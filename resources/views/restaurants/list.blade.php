@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'restaurant/show', 'method' => 'post','enctype' => 'multipart/form-data', 'novalidate', 'class' => 'form.inline' ]) !!}
					<div class="row" >
						<div class="col-md-6" >
							<div class="form-group" >
								<label>Nombre</label>
								<input type="text" name="name" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" >
								<label>Ciudad</label>
								<input type="text" name="city" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group" >
						<button class="btn btn-default">Buscar</button>
						<a href="{{ route('restaurant.index') }}" class="btn btn-success">Todos</a>
						<a href="{{ route('restaurant.create') }}" class="btn btn-success">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-condensed table-striped table-bordered" >
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Description</th>
							<th>Direccion</th>
							<th>Ciudad</th>
							<th>Foto</th>
							<th>Opcion</th>
						</tr>
					</thead>
					<tbody>
						@foreach($restaurants as $restaurant)
							<tr>
								<td>{{ $restaurant->name }}</td>
								<td>{{ $restaurant->description }}</td>
								<td>{{ $restaurant->adress }}</td>
								<td>{{ $restaurant->city }}</td>
								<td><img src="{{ asset('images/'.$restaurant->photo) }}" height="40" width="30"></td>
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('restaurant.edit',['id' => $restaurant->id]) }}">Editar</a>
									<a class="btn btn-danger btn-xs" href="{{ route('restaurant/destroy', ['id' => $restaurant->id]) }}">Borrar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection