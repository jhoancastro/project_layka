@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'reservation.store', 'method' => 'post', 'novalidate' ]) !!}
					<div class="form-group">
						<label>Dia de la reserva</label>
						<input type="date" name="datere" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Restaurante</label>
						<input type="text" name="restaurant" class="form-control" required>
					</div>
					<div class="form-group">
						<button type="subit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection