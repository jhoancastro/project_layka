@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'reservation/show', 'method' => 'post', 'novalidate', 'class' => 'form.inline' ]) !!}
					<div class="form-group" >
						<label>Restaurante</label>
						<input type="text" name="restaurant" class="form-control">
					</div>
					<div class="form-group" >
						<button class="btn btn-default">Buscar</button>
						<a href="{{ route('reservation.index') }}" class="btn btn-success">Todos</a>
						<a href="{{ route('reservation.create') }}" class="btn btn-success">Crear</a>
					</div>
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-condensed table-striped table-bordered" >
					<thead>
						<tr>
							<th>Dia de la reserva</th>
							<th>Restaurante</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($reservations as $reservation)
							<tr>
								<td>{{ $reservation->datere }}</td>
								<td>{{ $reservation->restaurant }}</td>
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('reservation.edit',['id' => $reservation->id]) }}">Editar</a>
									<a class="btn btn-danger btn-xs" href="{{ route('reservation/destroy', ['id' => $reservation->id]) }}">Borrar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection