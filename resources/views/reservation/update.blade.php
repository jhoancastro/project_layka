@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($movie, ['route' => ['movie.update', $movie->id], 'method' => 'put', 'novalidate' ]) !!}
					<div class="form-group">
						<label>Dia de la reserva</label>
						<input type="date" name="datere" class="form-control" value="{{ $movie->datere }}" required>
					</div>
					<div class="form-group">
						<label>Restaurante</label>
						<input type="text" name="restaurant" class="form-control" value="{{ $movie->restaurant }}" required>
					</div>
					<div class="form-group">
						<button type="subit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>
	</section>
@endsection