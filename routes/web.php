<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('restaurant', 'RestaurantController');
Route::get('restaurant/destroy/{id}', ['as' => 'restaurant/destroy', 'uses' => 'RestaurantController@destroy']);
Route::post('restaurant/show', ['as' => 'restaurant/show', 'uses' => 'RestaurantController@show'] );
Auth::routes();

Route::resource('reservation', 'ReservationController');
Route::get('reservation/destroy/{id}', ['as' => 'reservation/destroy', 'uses' => 'ReservationController@destroy']);
Route::post('reservation/show', ['as' => 'reservation/show', 'uses' => 'ReservationController@show'] );
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
